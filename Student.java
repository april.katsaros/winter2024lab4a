public class Student {
	private String name;
	private int age;
	private String studentId;
	private int amountLearnt;
	
	public Student(String name, int age){
		this.name = name;
		this.age = age;
		this.studentId = "0000000";
		this.amountLearnt = 0;
	}
	public void sayName(){
		System.out.println("Hello " + this.name + "!");
	}
	public void sayStudentId(){
		System.out.println(this.name + "'s student id is: " + this.studentId);
	}
	public void study(int amountStudied){
		this.amountLearnt = amountStudied;
	}
	
	public String getName(){
		return this.name;
	}
	public int getAge(){
		return this.age;
	}
	public String getStudentId(){
		return this.studentId;
	}
	public int getAmountLearnt(){
		return this.amountLearnt;
	}
	
	public void setStudentId(String studentId){
		this.studentId = studentId;
	}
}
	