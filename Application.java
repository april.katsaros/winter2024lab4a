import java.util.Scanner;

public class Application{
	public static void main(String[] args){
		Student april = new Student("April", 18);
		System.out.println(april.getName());
		// april.setName("April");
		System.out.println(april.getName());
		// april.setAge(18);
		// april.setStudentId("2237773");
		
		Student tin = new Student("Tin", 17);
		// tin.setName("Tin");
		// tin.setAge(17);
		// tin.setStudentId("2337773");
		
		System.out.println(april.getName());
		System.out.println(tin.getStudentId());
		
		april.sayName();
		tin.sayStudentId();
		
		Student[] section3 = new Student[3];
		
		section3[0] = april;
		section3[1] = tin;
		
		System.out.println(section3[0].getName());
		
		section3[2] = new Student("Tinothy", 17);
		
		// section3[2].setName("Tinothy");
		// section3[2].setAge(17);
		// section3[2].setStudentId("3337773");
		
		System.out.println(section3[2].getName() + ", " + section3[2].getAge() + ", " + section3[2].getStudentId());
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter a whole number:");
		int amountStudied = Integer.parseInt(reader.nextLine());
		
		for (int i = 0; i < section3.length; i++){
			System.out.println(section3[i].getAmountLearnt());
		}
		
		section3[2].study(amountStudied);
		section3[2].study(amountStudied);
		
		for (int i = 0; i < section3.length; i++){
			System.out.println(section3[i].getAmountLearnt());
		}
		Student tinny = new Student("Tinny", 17);
		tinny.setStudentId("3333333");
		System.out.println(tinny.getName());
		System.out.println(tinny.getAge());
		System.out.println(tinny.getStudentId());
		System.out.println(tinny.getAmountLearnt());
	}
}